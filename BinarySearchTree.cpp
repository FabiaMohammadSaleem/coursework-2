#include "BinarySearchTree.h"
#include <iostream>
#include <vector>
#include "Book.h"
#include <string>
#include <fstream>
#include <sstream>

BinarySearchTree::BinarySearchTree()
        {
           root = NULL;
        }

std::vector<std::string> BinarySearchTree::StringDivider(std::string book_vb, 
        char separator){

              std::vector<std::string> words;
              std::stringstream ss(book_vb);
              
              std::string str;
              while(getline(ss, str, separator)){
                
                  words.push_back(str);
              }
              return words;
        }
 void BinarySearchTree::Read_from_file(std::string filename){
        //Declaring book object and referencing it as a pointer  
            Book b;
            std::ifstream f;
            std::string sentences = " ";
            f.open(filename, std::ios_base::in);
            if(f.is_open()){
              while(f.good()){
                getline(f, sentences);
                std::vector<std::string> v = StringDivider(sentences,'\t');
                //sets as book object and insert the file contents to the data structure
                if(f.good()){  
                  b.setData(v[0], v[1], v[2], (v[3]));
                  insert(b);
                }
              }  
            f.close();
            }
            std::cout<<"\nFile inserted to the data structure"; 
        }

 void BinarySearchTree::insert_to_book_file(std::string b_title,std::string b_author,std::string b_ph,std::string b_qty,std::string filename) {
            std::fstream f;
            
            
            Book b;
            b.setData(b_title,b_author,b_ph,(b_qty));
            
            insert(b);
            
            f.open(filename,std::ios_base::out|std::ios::app);
            
            f<<b_title<<'\t'<<b_author<<'\t'<<b_ph<<'\t'<<b_qty<<'\n';
            f.close();
        }
void BinarySearchTree::delete_From_book_File(std::string name,std::string filename){
        	std::string line;
        	std::ifstream myfile;
        	myfile.open(filename);
        	std::ofstream temp;
        	temp.open("temp.txt");

        	// reads the line to be deleted
        	while (getline(myfile, line)) {
				std::string title = line.substr(0, line.find('\t'));
        	    if (title != name)
        	        temp << line << std::endl;
        	}
          
        	myfile.close();
        	temp.close();
        	remove(filename.c_str());
        	rename("temp.txt", filename.c_str());
        }


typedef std::string treeType;



// Smaller elements go left
// larger elements go right
bool BinarySearchTree::insert(Book p)
{
    tree_node* t = new tree_node;
    tree_node* parent;
    t->title = p.getTitle();
    t->author = p.getAuthor();
    t->ISBN = p.getISBN();
    t->quantity = p.getquantity();
    t->left = NULL;
    t->right = NULL;
    parent = NULL;

    // is this a new tree?
    if(isEmpty())
     root = t;
    else
    {
        //Note: ALL insertions are as leaf nodes
        tree_node* curr;
        curr = root;
        // Find the Node's parent
        while(curr)
        {
            parent = curr;
            if(t->title > curr->title) curr = curr->right;
            else curr = curr->left;
        }

        if(t->title < parent->title)
           parent->left = t;
        else
           parent->right = t;
    }
    return true;
}

bool BinarySearchTree::remove_book(std::string p)
{
    //Locate the element
    bool found = false;
    if(isEmpty())
    {
        std::cout<<" This Tree is empty! "<<std::endl;
        return false;
    }

    tree_node* curr;
    tree_node* parent;
    curr = root;

    while(curr != NULL)
    {
         if(curr->title == p)
         {
            found = true;
            break;
         }
         else
         {
             parent = curr;
             if(p>curr->title) curr = curr->right;
             else curr = curr->left;
         }
    }
    if(!found)
		 {
        std::cout<<" Data not found! "<<std::endl;
        return false;
    }


		 // 3 cases :
    // 1. We're removing a leaf node
    // 2. We're removing a node with a single child
    // 3. we're removing a node with 2 children

    // Node with single child
    if((curr->left == NULL && curr->right != NULL)|| (curr->left != NULL
&& curr->right == NULL))
    {
       if(curr->left == NULL && curr->right != NULL)
       {
           if(parent->left == curr)
           {
             parent->left = curr->right;
             delete curr;
           }
           else
           {
             parent->right = curr->right;
             delete curr;
           }
       }
       else // left child present, no right child
       {
          if(parent->left == curr)
           {
             parent->left = curr->left;
             delete curr;
           }
           else
           {
             parent->right = curr->left;
             delete curr;
           }
       }
     return true;
    }

		 //We're looking at a leaf node
		 if( curr->left == NULL && curr->right == NULL)
    {
        if(parent->left == curr) parent->left = NULL;
        else parent->right = NULL;
		 		 delete curr;
		 		 return true;
    }


    //Node with 2 children
    // replace node with smallest value in right subtree
    if (curr->left != NULL && curr->right != NULL)
    {
        tree_node* chkr;
        chkr = curr->right;
        if((chkr->left == NULL) && (chkr->right == NULL))
        {
            curr = chkr;
            delete chkr;
            curr->right = NULL;
        }
        else // right child has children
        {
            //if the node's right child has a left child
            // Move all the way down left to locate smallest element

            if((curr->right)->left != NULL)
            {
                tree_node* lcurr;
                tree_node* lcurrp;
                lcurrp = curr->right;
                lcurr = (curr->right)->left;
                while(lcurr->left != NULL)
                {
                   lcurrp = lcurr;
                   lcurr = lcurr->left;
                }
		curr->title = lcurr->title;
                delete lcurr;
                lcurrp->left = NULL;
           }
           else
           {
               tree_node* tmp;
               tmp = curr->right;
               curr->title = tmp->title;
	       curr->right = tmp->right;
               delete tmp;
           }

        }
		 return true;
    }

  return true;
}


bool BinarySearchTree::search(std::string title)
{
     bool found = false;
    if(isEmpty())
    {
        std::cout<<" This Tree is empty! "<<std::endl;
        return false;
    }

    tree_node* curr;
    tree_node* parent;
    curr = root;

    while(curr != NULL) {
         if(curr->title == title) {
            found = true;
            std::cout << "The Details for " << title << " is\n " << curr->title<<"\n"<<curr->author<<"\n" << curr->ISBN<<"\n"<< curr->quantity<< std::endl;
            break;
         }
         else {
             parent = curr;
             if(title>curr->title) curr = curr->right;
             else curr = curr->left;
         }
    }
    if(!found)
		 {
        std::cout<<" Data not found! "<<std::endl;
        return false;
    }

    return true;
}


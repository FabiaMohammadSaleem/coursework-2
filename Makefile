CXX = g++
CXXFLAGS = -g -Wall -Wextra

.PHONY: all
all : main
main: main.cpp Book.o BinarySearchTree.o
	$(CXX) $(CXXFLAGS) -o $@ $^
Book.o: Book.cpp  Book.h
	$(CXX) $(CXXFLAGS) -c $<
BinarySearchTree.o: BinarySearchTree.cpp  BinarySearchTree.h
	$(CXX) $(CXXFLAGS) -c $<  
.PHONY : clean
clean:
	rm -rf *~ *.o main

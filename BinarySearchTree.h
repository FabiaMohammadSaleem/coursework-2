#include <vector>
#include <string>
#include "Book.h"

class BinarySearchTree
{
    private:
        struct tree_node
        {
           tree_node* left;
           tree_node* right;
           //treeType data;
          //  Book data;
          std::string title;
          std::string author;
          std::string ISBN;
          std::string quantity;

        };
        tree_node* root;

    public:
        BinarySearchTree();

        bool isEmpty() const { return root==NULL; }
        bool insert(Book p);
        bool remove_book(std::string title);
        bool search(std::string title);
        std::vector<std::string> StringDivider(std::string book_vb, 
        char separator);
 
        void Read_from_file(std::string filename);

        void insert_to_book_file(std::string b_title,std::string b_author,std::string b_ph,std::string b_qty,std::string filename) ;

        void delete_From_book_File(std::string name,std::string filename);
};



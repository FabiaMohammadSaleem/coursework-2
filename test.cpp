#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "BinarySearchTree.cpp"
#include "Book.cpp"
#include <iostream>


//Testing the main functionalities (Search)
TEST_CASE("TestSearch", "[Test mainFunctions]")
{

std::string file_name = "books.txt";
    BinarySearchTree b;
    b.Read_from_file(file_name);
    Book bk;

  REQUIRE(b.search("High-Performance Computing") == true);

}

//Testing the main functionalities (Add)
TEST_CASE("TestAdd", "[TestAdd]")
{

std::string file_name = "books.txt";
    BinarySearchTree b;
    b.Read_from_file(file_name);
    Book bk;
  
  bk.setData("Introduction To Cybersecurity", "Jack Black", "43732346734", "2");
  REQUIRE(b.insert(bk) == true);
}

//Testing the main functionalities (Remove)
TEST_CASE("TestRemove", "[Test mainFunctions]")
{

std::string file_name = "books.txt";
    BinarySearchTree b;
    b.Read_from_file(file_name);
    Book bk;

  bk.setData("Introduction To Cybersecurity", "Jack Black", "43732346734", "2");
  b.insert(bk);

  REQUIRE(b.remove_book("Introduction To Cybersecurity") == true);
  std::cout << std::endl;

}